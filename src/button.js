import React, { Component } from 'react';
import './button.scss';

class Button extends Component {
    isType1Operator = () => {
        const value = this.props.value;
        return isNaN(value) && (value === "C" || value === "+/-" || value === "%")
    }

    isType2Operator = () => {
        const value = this.props.value;
        return isNaN(value) && (value === "/" || value === "x" || value === "-" || value === "+" || value === "=")
    }
    render() {
        const selectButtonStyle = this.isType1Operator() ? "buttonType1" : this.isType2Operator() ? "buttonType2" : null
        return (
            <button className={`buttonContainer ${selectButtonStyle}`} onClick={() => this.props.onButtonClick(this.props.value)}
            style={this.props.buttonStyle}>
                <span>{this.props.value}</span>
            </button>

        )
    }
}

export default Button;