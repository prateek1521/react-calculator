import React, { Component } from 'react';
import './App.scss';
import Button from './button';
class App extends Component {
    state = {
        inputValue : 0
    }

    onButtonClick = (value) => {
        this.setState({inputValue: value})
    }
    render() {
        return (
            <div className="mainContainer">
                <section>
                    <input value={this.state.inputValue} />
                    <div> 
                        <Button value="C" onButtonClick={this.onButtonClick} />
                        <Button value="+/-" onButtonClick={this.onButtonClick} />
                        <Button value="%" onButtonClick={this.onButtonClick} />
                        <Button value="/" onButtonClick={this.onButtonClick} />
                    </div>

                    <div> 
                        <Button value="7" onButtonClick={this.onButtonClick} />
                        <Button value="8" onButtonClick={this.onButtonClick} />
                        <Button value="9" onButtonClick={this.onButtonClick} />
                        <Button value="x" onButtonClick={this.onButtonClick} />
                    </div>

                    <div> 
                        <Button value="4" onButtonClick={this.onButtonClick} />
                        <Button value="5" onButtonClick={this.onButtonClick} />
                        <Button value="6" onButtonClick={this.onButtonClick} />
                        <Button value="-" onButtonClick={this.onButtonClick} />
                    </div>

                    <div> 
                        <Button value="1" onButtonClick={this.onButtonClick} />
                        <Button value="2" onButtonClick={this.onButtonClick} />
                        <Button value="3" onButtonClick={this.onButtonClick} />
                        <Button value="+" onButtonClick={this.onButtonClick} />
                    </div>

                    <div> 
                        <Button value="0" buttonStyle = {{flexGrow: 1}} onButtonClick={this.onButtonClick} />
                        <Button value="." onButtonClick={this.onButtonClick} />
                        <Button value="=" onButtonClick={this.onButtonClick} />
                    </div>
                </section>
            </div>
        );
    }
}

export default App;
